FROM golang:1.22.1 AS builder

LABEL maintainer="clseibold"

RUN apt update -y
RUN apt install -y build-essential git libasound2-dev

RUN git clone https://gitlab.com/clseibold/scroll-term.git && cd scroll-term && go mod download && cd .. && rm -rf ./scroll-term/
